import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

typedef OnPositionChange = Function(Duration oldPosition,
    Duration currentPosition, VideoPlayerController controller);
typedef OnCheckPoint = Future<bool> Function(
    int checkpoint, VideoPlayerController controller);

class LWVideoPlayer extends StatefulWidget {
  final double aspectRatio;
  final String dataSource;
  final List<int> checkpoints;
  final bool isLocal;
  final OnPositionChange onPositionChange;
  final OnCheckPoint onCheckPoint;

  /// Only set for asset videos. The package that the asset was loaded from.
  final String package;

  LWVideoPlayer({
    Key key,
    @required this.dataSource,
    this.aspectRatio = 16 / 9,
    this.checkpoints,
    this.isLocal = false,
    this.package,
    this.onPositionChange,
    this.onCheckPoint,
  }) : super(key: key);

  @override
  _LWVideoPlayerState createState() => _LWVideoPlayerState();
}

class _LWVideoPlayerState extends State<LWVideoPlayer> {
  VideoPlayerController _videoController;
  ChewieController _chewieController;
  Duration prePosition;

  bool get isLocal => widget.isLocal;

  String get package => widget.package;

  double get aspectRatio => widget.aspectRatio;

  String get dataSource => widget.dataSource;

  List<int> get checkpoints => widget.checkpoints ?? [];

  OnPositionChange get onPositionChange => widget.onPositionChange;

  OnCheckPoint get onCheckPoint => widget.onCheckPoint;

  @override
  void initState() {
    super.initState();
    sortCheckpoints();
    initializePlayer();
  }

  sortCheckpoints() {
    checkpoints.sort();
  }

  initializePlayer() async {
    _videoController = isLocal
        ? VideoPlayerController.asset(dataSource, package: package)
        : VideoPlayerController.network(dataSource);

    await _videoController.initialize();
    _chewieController = ChewieController(
      videoPlayerController: _videoController,
      allowedScreenSleep: false,
      // aspectRatio: aspectRatio,
      autoPlay: true,
      fullScreenByDefault: false,
      placeholder: Container(
        color: Colors.black,
      ),
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.white),
          ),
        );
      },
    );

    _videoController.addListener(() async {
      final currentPosition = await _videoController.position;
      if (_isPositionChanged(prePosition, currentPosition)) {
        final tmpPrePosition = prePosition;
        prePosition = currentPosition;
        // print('Phungtd: onPositionChanged .... ');
        if (onPositionChange != null) {
          onPositionChange(tmpPrePosition, currentPosition, _videoController);
        }
        // final index = checkpoints.indexOf(currentPosition.inSeconds);
        // if (index != -1) {
        //   final pass = await onCheckPoint(checkpoints[index], _videoController);
        //   if (pass) {
        //     checkpoints.removeAt(index);
        //   }
        // }
        if (checkpoints.length >= 1 && currentPosition.inSeconds >= checkpoints.first) {
          final pass = await onCheckPoint(checkpoints.first, _videoController);
          if (pass) {
            checkpoints.removeAt(0);
          }
        }
      }
      // print('Phungtd: position : $currentPosition');
    });

    setState(() {});
  }

  @override
  void dispose() {
    _videoController?.dispose();
    _chewieController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: aspectRatio,
      child: _renderPlayerWidget(),
    );
  }

  Widget _renderPlayerWidget() {
    if (_chewieController != null &&
        _chewieController.videoPlayerController.value.initialized) {
      return Chewie(controller: _chewieController);
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          SizedBox(height: 20),
          CircularProgressIndicator(),
        ],
      );
    }
  }

  bool _isPositionChanged(Duration p1, Duration p2) {
    final p1InMilis = p1?.inMilliseconds ?? 0;
    final p2InMilis = p2?.inMilliseconds ?? 0;
    return (p1InMilis - p2InMilis).abs() >= 500;
  }
}
