import 'package:flutter/material.dart';
import 'package:lw_video_player/lw_video_player.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MainScreen(),
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          child: Text('Go to home'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MyHomePage(),
              ),
            );
          },
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Video Demooo',
      color: Colors.blue,
      home: Scaffold(
        body: ListView(
          children: [
            Container(
              color: Colors.red,
              height: 50,
            ),
            LWVideoPlayer(
              aspectRatio: 16 / 9,
              // dataSource: 'http://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_20mb.mp4',
              dataSource: 'https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8',
              onPositionChange: (old, current, controller) {
                print(
                    'Phungtd: position Changed from ${old?.inMilliseconds} to ${current.inMilliseconds}');
              },
              checkpoints: [5, 10, 15],
              onCheckPoint: (second, controller) async {
                await controller.pause();
                await showDialog(
                    context: context,
                    child: AlertDialog(
                      title: Text("My Dialog title"),
                      content: Text("Hello World"),
                    ));
                controller.play();
                return true;
                // return false;
              },
            ),
            Container(
              color: Colors.blue,
              height: 50,
            ),
          ],
        ),
      ),
    );
  }
}
